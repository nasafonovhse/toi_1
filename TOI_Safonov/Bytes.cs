﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TOI_Safonov
{
    class Bytes
    {
        public static void Reader(string fileName)
        {
            using (FileStream fileFile = new FileStream($"{fileName}", FileMode.Open, FileAccess.Read))
            {
                byte[] fileBytes = new byte[fileFile.Length];
                int l = (int)fileFile.Length;
                int x = 0;
                while (l > 0)
                {
                    int n = fileFile.Read(fileBytes, x, l);
                    if (n == 0)
                        break;
                    x += n;
                    l -= n;
                }
                l = fileBytes.Length;
                List<ByteCount> fileCount = new List<ByteCount>();
                fileCount = ByteCount.Counter(fileBytes);
                var w = from a in fileCount
                        orderby int.Parse(a.Byte) descending
                        select a;
                Console.WriteLine($"{fileName}");
                Console.WriteLine();
                double hx = 0;
                foreach (var item in w)
                {
                    Console.WriteLine($"{fileName} || Byte {item.Byte}   Count: {item.Count}   I(Xi) = {item.Hx}");
                    hx += item.Hx;
                }                
                Console.WriteLine();
                Console.WriteLine($"Энтропия этого источника: {hx}   ({fileName})");
                Console.WriteLine();

            }
        }
    }
}
