﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TOI_Safonov
{
    class ByteCount
    {
        public string Byte { get; set; }
        public int Count { get; set; }
        public double Hx { get; set; }
    
        public static List<ByteCount> Counter(byte[] bytes)
        {
            List<ByteCount> fileAll = new List<ByteCount>();
            foreach (byte _byte in bytes)
            {
                ByteCount txtB = new ByteCount
                {
                    Byte = _byte.ToString(),
                    Count = 1,
                };
                fileAll.Add(txtB);
            }
            List<ByteCount> fileCount = new List<ByteCount>();
            foreach (var b in fileAll)
            {
                if (fileCount.Find(s => s.Byte == b.Byte) == null)
                {
                    b.Count = fileAll.FindAll(by => by.Byte == b.Byte).Count;
                    fileCount.Add(b);
                    b.Hx = Math.Round((((double)b.Count / (double)fileAll.Count) * Math.Log((1 / ((double)b.Count / (double)fileAll.Count)), 2.0)), 3);
                }
            }
            return fileCount;
        }
    }
}
